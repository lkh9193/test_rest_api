using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;

namespace Test.Rest.API
{
    public class Members
    {
        [Key]
        public string? uuid { get; set; }
        public string? name { get; set; }
        public string? nickname { get; set; }
        public string? address { get; set; }
        public int? status { get; set; }
        public DateTime? created_at { get; set; }
        public DateTime? updated_at { get; set; }
        public string? created_by { get; set; }
        public string? updated_by { get; set; }
    }

    public class MembersContext : DbContext
    {
        private string _connection = "";

        public MembersContext(string connection)
        {
            _connection = connection;
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Members>().ToTable("member");
        }
        public DbSet<Members> MemberDatas { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //var ConnectionString = "server=127.0.0.1;port=3306;user=myuserid;password=thispasssmy00;pooling=false;database=test_api;charset=utf8;";
        
            var mySQL = new MySqlServerVersion(new Version(10, 9, 00));
            optionsBuilder.UseMySql(_connection, mySQL);
            // optionsBuilder.UseMySql(_connection, mySQL, builder =>
            // {
            //     builder.EnableRetryOnFailure(
            //         maxRetryCount: 5, // Number of retry attempts
            //         maxRetryDelay: TimeSpan.FromSeconds(30), // Delay between retries
            //         errorNumbersToAdd: null // List of MySQL error numbers to retry on, or null for all errors
            //     );
            // });
            optionsBuilder.LogTo(Console.WriteLine, LogLevel.Debug);
            optionsBuilder.EnableSensitiveDataLogging();
            optionsBuilder.EnableDetailedErrors();
          
        }
    }
}
