using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Nodes;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Test.Rest.API;

namespace Test.Rest.API.Controllers 
{
    [Route("api/[controller]")]
    [ApiController]
    public class MembersController : ControllerBase
    {

       [HttpGet]
        public IActionResult GetAllMembers()
        {
            List<Members> membersList = new List<Members>();
            
            using (var db = new MembersContext(ConnectionString()))
            {
                membersList = db.MemberDatas.ToList();
            }
            
            if (membersList.Count == 0)
            {
                return NotFound("No members found.");
            }

            return Ok(membersList);
        }


        [HttpGet("{uuid}")]
        public IActionResult GetMemberById(string uuid)
        {
            Members member;
            
            using (var db = new MembersContext(ConnectionString()))
            {
                member = db.MemberDatas.FirstOrDefault(m => m.uuid == uuid);
            }
            
            if (member == null)
            {
                return NotFound($"No member found with UUID: {uuid}");
            }

            return Ok(member);
        }

        [HttpPost]
        // public IActionResult InsertMember(string name, string nickname, string address, string createdBy)
        public IActionResult InsertMember([FromBody] MemberCreateRequest request)
        {
            Members newMember = new Members()
            {
                uuid = Guid.NewGuid().ToString("N"),
                name = request.name,
                nickname = request.nickname ?? string.Empty, 
                address = request.address,
                status = 1, 
                created_at = DateTime.Now,
                created_by = request.createdBy
            };

            using (var db = new MembersContext(ConnectionString())) 
            {
                db.Database.EnsureCreated();
                db.MemberDatas.Add(newMember);
                if (db.SaveChanges() != 1)
                {
                    var errorData = new JsonObject();
                    errorData["Status"] = 422;
                    errorData["Message"] = "Member Creation : Fail.";
            
                    return BadRequest(errorData);
                }
            }

            return Ok("Member Creation : Successful."); 
        }

        [HttpPut("{uuid}")]
        public IActionResult UpdateMember(string uuid, [FromBody] MemberUpdateRequest request)
        {
            using (var db = new MembersContext(ConnectionString()))
            {
                var member = db.MemberDatas.FirstOrDefault(m => m.uuid == uuid);
                
                if (member == null)
                {
                    return NotFound($"No member found with ID: {uuid}");
                }

                member.name = request.name ?? member.name;
                member.nickname = request.nickname ?? member.nickname;
                member.address = request.address ?? member.address;
                member.status = request.status ?? member.status;
                member.updated_at = DateTime.Now;
                member.updated_by = request.updated_by ?? member.updated_by;

                db.MemberDatas.Update(member);
                if (db.SaveChanges() != 1)
                {
                    return BadRequest("Error updating the member.");
                }
            }

            return Ok("Member updated successfully.");
        }

        [HttpDelete("{uuid}")]
        public IActionResult DeleteMember(string uuid)
        {
            using (var db = new MembersContext(ConnectionString()))
            {
                var member = db.MemberDatas.FirstOrDefault(m => m.uuid == uuid);
                
                if (member == null)
                {
                    return NotFound($"No member found with ID: {uuid}");
                }

                db.MemberDatas.Remove(member);
                
                if (db.SaveChanges() != 1)
                {
                    return BadRequest("Error deleting the member.");
                }
            }

            return Ok("Member deleted successfully.");
        }


        protected string ConnectionString()
        {
            /*
                for container mariadb
            */
            return "server=test_rest_api_mariadb_1;port=3306;user=myuserid;password=thispasssmy00;pooling=false;database=test_api;charset=utf8;";

            return "server=127.0.0.1;port=3306;user=myuserid;password=thispasssmy00;pooling=false;database=test_api;charset=utf8;";
        }

    }
   
}

    public class MemberUpdateRequest
    {
        public string name { get; set; }
        public string nickname { get; set; }
        public string address { get; set; }
        public sbyte? status { get; set; }
        public string updated_by { get; set; }
    }

        public class MemberCreateRequest
    {
        public string name { get; set; }
        public string nickname { get; set; }
        public string address { get; set; }
        public string createdBy { get; set; }
    }